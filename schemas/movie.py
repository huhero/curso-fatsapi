from pydantic import BaseModel, Field
from typing import Optional, List


class Movie(BaseModel):
    id: Optional[int] = None
    title: str = Field(min_length=3, max_length=15)
    overview: str = Field(min_length=20, max_length=250)
    year: int = Field(le=2023)
    rating: float = Field(ge=0.1, le=1.0)
    category: str = Field(min_length=3, max_length=20)

    class Config:
        schema_extra = {
            "example": {
                "id": 1,
                "title": "Avatar",
                "overview": "En un exuberante planeta llamado Pandora viven los Navi, seres que ...",
                "year": "2009",
                "rating": 0.78,
                "category": "Acción",
            }
        }
